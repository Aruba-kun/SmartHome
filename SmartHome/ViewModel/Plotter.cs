﻿using OxyPlot;

namespace SmartHome
{
    public class Plotter
    {
        public static PlotModel Sensor { get; set; }
        private OxyPlot.Series.LineSeries LS;

        public Plotter()
        {
            Sensor = new PlotModel();
        }
    }
}
